package fibonacci;




	import java.text.DecimalFormat;
	import java.util.Arrays;
	import java.util.Scanner;
	public class fibonacci {


	    public static Scanner scanner= new Scanner(System.in);
	    public static void main(String[] args) {

	        int  n, m, i, cpt;
	        double Um_1, Um_2, Um, p;
	        DecimalFormat pi = new DecimalFormat ( ) ;
	        pi.setMaximumFractionDigits ( 8 );
	        Afficher("Donnez la valeur de n, n > 2");
	        n = Saisir();
	        Um_2 = 0.0;
	        Um_1 = 1.0;
	        m = 2;
	        Um = 0;
	        i = 0;
	        p=0;

	        double fib [] = new double  [n];
	        double result [] = new double[n];


	            while (m <= n) {
	                Um = Um_1 + Um_2;
	                Um_2 = Um_1;
	                Um_1 = Um;
	                m = m + 1;
	                fib[i] = Um;
	                p = Um_1/Um_2;
	                i++;
	                result[i]=p;
	                }
	                if(n<10) {
	                    cpt=0;
	                }
	                else {cpt = n-10;
	                }
	                for (cpt=cpt ; cpt<n-1;cpt++) {
	                    System.out.println(pi.format(fib[cpt]) + ", " +pi.format(result[cpt]));
	                }

	        System.out.println(Arrays.toString(fib));
	        System.out.println(Arrays.toString(result));

	        Afficher("Le terme Um = "+ Um);

	    }
	    public static int Saisir() {
	        int entier = 0;
	        boolean b = false;
	        do {
	            System.out.println("Veuillez saisir un nombre entier");
	            try {
	                entier = scanner.nextInt();
	                if(entier>= 2) {
	                    b = true;}

	            }catch (Exception e ) {
	                System.out.println("Erreur");
	                scanner.nextLine();
	            }
	        }while (b == false);
	        return entier;
	    }
	    public static void Afficher(String message) {
	        System.out.println(message);

	    }


	}

